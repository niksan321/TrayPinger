﻿using System.Windows;
using Prism.Commands;
using Prism.Mvvm;
using TrayPinger.Services;

namespace TrayPinger.ViewModels;

public class NotifyIconViewModel : BindableBase
{
    private readonly TrayService _trayService;
    private readonly PrismApplicationEx _app;

    public DelegateCommand ShowWindowCommand { get; set; }
    public DelegateCommand HideWindowCommand { get; set; }
    public DelegateCommand ExitApplicationCommand { get; set; }

    public NotifyIconViewModel(TrayService trayService)
    {
        _trayService = trayService;
        _app = (PrismApplicationEx)Application.Current;

        ShowWindowCommand = new DelegateCommand(ShowWindowExecute, ShowWindowCanExecute);
        HideWindowCommand = new DelegateCommand(HideWindowExecute, HideWindowCanExecute);
        ExitApplicationCommand = new DelegateCommand(ExitApplicationExecute);
    }

    public void Invalidate()
    {
        ShowWindowCommand.RaiseCanExecuteChanged();
        HideWindowCommand.RaiseCanExecuteChanged();
    }

    private bool HideWindowCanExecute()
    {
        return _app.MainWindow != null;
    }

    private void HideWindowExecute()
    {
        _trayService.Hide();
        Invalidate();
    }

    private void ExitApplicationExecute()
    {
        _app.Shutdown();
    }

    private void ShowWindowExecute()
    {
        _trayService.Show();
        Invalidate();
    }

    private bool ShowWindowCanExecute()
    {
        if (_app.MainWindow is { DataContext: null })
        {
            return true;
        }

        return _app.MainWindow == null;
    }
}