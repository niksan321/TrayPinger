﻿using System.Windows.Input;
using Prism.Commands;
using Prism.Events;
using Prism.Ioc;
using Prism.Regions;
using TrayPinger.Constants;
using TrayPinger.Events;
using TrayPinger.Langs;
using TrayPinger.Services;
using TrayPinger.Views;

namespace TrayPinger.ViewModels;

public class EditTargetViewModel : BaseViewModel
{
    private readonly IRegionManager _regionManager;
    private readonly MenuService _menuService;
    private readonly IEventAggregator _eventAggregator;

    private TargetItemViewModel _model;
    public TargetItemViewModel Model
    {
        get => _model;
        set => SetProperty(ref _model, value);
    }

    public ICommand SaveCommand { get; }

    public EditTargetViewModel(IRegionManager regionManager, IContainerProvider container,
        IEventAggregator eventAggregator, MenuService menuService)
        : base(container)
    {
        _regionManager = regionManager;
        _menuService = menuService;
        _eventAggregator = eventAggregator;

        SaveCommand = new DelegateCommand(Save);
    }

    public override void OnNavigatedTo(NavigationContext navigationContext)
    {
        Model = (TargetItemViewModel)navigationContext.Parameters[nameof(TargetItemViewModel)];
        _menuService.MainTitleId = "EditTargetItemViewTitle";
    }

    private void Save()
    {
        _eventAggregator.GetEvent<TargetItemChangedEvent>().Publish(Model);
        _regionManager.RequestNavigate(RegionConstants.Content, nameof(TargetListView));
    }
}