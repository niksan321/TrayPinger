﻿using System.Windows.Input;
using Prism.Commands;
using Prism.Ioc;
using Prism.Regions;
using TrayPinger.Constants;
using TrayPinger.Helpers;
using TrayPinger.Models;
using TrayPinger.Services;
using TrayPinger.Views;

namespace TrayPinger.ViewModels;

public class TargetListViewModel : BaseViewModel
{
    private readonly MenuService _menuService;
    private readonly IRegionManager _regionManager;
    private readonly DataSeedHelper _dataSeedHelper;
    private readonly TargetItemStorageService _storageService;
    public SchedulerService SchedulerService { get; }

    private TargetItemViewModel _selectedItemViewModel;
    public TargetItemViewModel SelectedItemViewModel
    {
        get => _selectedItemViewModel;
        set => SetProperty(ref _selectedItemViewModel, value);
    }

    public ObservableCollectionEx<TargetItemViewModel> Targets => _storageService.Items;

    public ICommand AddCommand { get; }
    public ICommand EditCommand { get; }

    public TargetListViewModel(IContainerProvider container, IRegionManager regionManager, DataSeedHelper dataSeedHelper,
        TargetItemStorageService storageService, MenuService menuService, SchedulerService schedulerService)
    : base(container)
    {
        _menuService = menuService;
        _regionManager = regionManager;
        _dataSeedHelper = dataSeedHelper;
        _storageService = storageService;
        SchedulerService = schedulerService;

        AddCommand = new DelegateCommand(AddNewTarget);
        EditCommand = new DelegateCommand(EditTarget, CanEditTarget);
    }

    private bool CanEditTarget()
    {
        return SelectedItemViewModel != null;
    }

    private void EditTarget()
    {
        var parameters = new NavigationParameters { { nameof(TargetItemViewModel), SelectedItemViewModel } };
        _regionManager.RequestNavigate(RegionConstants.Content, nameof(EditTargetView), parameters);
    }

    public void AddNewTarget()
    {
        var newTarget = _dataSeedHelper.GetDefaultTargetItemViewModel(Targets.Count + 1);
        _storageService.Add(newTarget);
    }

    public override void OnNavigatedTo(NavigationContext navigationContext)
    {
        _menuService.MainTitleId = nameof(Langs.Langs.TargetListViewTitle);
    }
}