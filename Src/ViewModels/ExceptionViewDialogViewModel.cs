﻿using System.Windows;
using System.Windows.Input;
using Prism.Commands;
using Prism.Mvvm;
using TrayPinger.Helpers;

namespace TrayPinger.ViewModels;

public class ExceptionViewDialogViewModel : BindableBase
{
    private string _message;
    public string Message
    {
        get => _message;
        set => SetProperty(ref _message, value);
    }

    public ICommand CloseCommand { get; set; }
    public ICommand CopyCommand { get; set; }

    public ExceptionViewDialogViewModel()
    {
        CloseCommand = new DelegateCommand(CloseExecute);
        CopyCommand = new DelegateCommand(CopyExecute);
    }

    private void CloseExecute()
    {
        DialogHostHelper.Close();
    }

    private void CopyExecute()
    {
        Clipboard.SetText(Message);
    }
}