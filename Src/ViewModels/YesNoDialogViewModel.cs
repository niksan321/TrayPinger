﻿using System.Windows.Input;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;

namespace TrayPinger.ViewModels;

internal class YesNoDialogViewModel : BindableBase, IDialogAware
{
    private string _title;
    public string Title
    {
        get => _title;
        set => SetProperty(ref _title, value);
    }

    private string _text;
    public string Text
    {
        get => _text;
        set => SetProperty(ref _text, value);
    }

    public ICommand YesCommand { get; set; }
    public ICommand NoCommand { get; set; }

    public event Action<IDialogResult> RequestClose;

    public YesNoDialogViewModel()
    {
        YesCommand = new DelegateCommand(YesCommandExecute);
        NoCommand = new DelegateCommand(NoCommandExecute);
    }

    private void NoCommandExecute()
    {
        RaiseRequestClose(new DialogResult(ButtonResult.No));
    }

    public virtual void RaiseRequestClose(IDialogResult dialogResult)
    {
        RequestClose?.Invoke(dialogResult);
    }

    private void YesCommandExecute()
    {
        RaiseRequestClose(new DialogResult(ButtonResult.Yes));
    }

    public bool CanCloseDialog() { return true; }

    public void OnDialogClosed() { }

    public void OnDialogOpened(IDialogParameters parameters)
    {
        Text = parameters.GetValue<string>(nameof(Text));
        Title = parameters.GetValue<string>(nameof(Title));
    }
}