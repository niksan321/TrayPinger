﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using MapsterMapper;
using Prism.Commands;
using Prism.Ioc;
using Prism.Regions;
using TrayPinger.Constants;
using TrayPinger.Models;
using TrayPinger.Services;
using TrayPinger.Views;
using Wpf.Tr;

namespace TrayPinger.ViewModels;

public class SettingsViewModel : BaseViewModel
{
    private readonly IRegionManager _regionManager;
    private readonly TranslateManager _translationManager;
    private readonly SettingsService _settingsService;
    private readonly IMapper _mapper;
    private readonly MenuService _menuService;

    private ObservableCollection<TargetItemViewModel> _targetItems;
    public ObservableCollection<TargetItemViewModel> TargetItems
    {
        get => _targetItems;
        set => SetProperty(ref _targetItems, value);
    }

    private bool _runOnStartUp;
    public bool RunOnStartUp
    {
        get => _runOnStartUp;
        set => SetProperty(ref _runOnStartUp, value);
    }

    private bool _autoStartPing;
    public bool AutoStartPing
    {
        get => _autoStartPing;
        set => SetProperty(ref _autoStartPing, value);
    }

    private bool _notify;
    public bool Notify
    {
        get => _notify;
        set => SetProperty(ref _notify, value);
    }

    private bool _startMinimized;
    public bool StartMinimized
    {
        get => _startMinimized;
        set => SetProperty(ref _startMinimized, value);
    }

    private bool _showDeletePrompt;
    public bool ShowDeletePrompt
    {
        get => _showDeletePrompt;
        set => SetProperty(ref _showDeletePrompt, value);
    }

    private bool _playErrorSound;
    public bool PlayErrorSound
    {
        get => _playErrorSound;
        set => SetProperty(ref _playErrorSound, value);
    }

    private string _language;
    public string Language
    {
        get => _language;
        set
        {
            if (SetProperty(ref _language, value))
            {
                var lang = TranslateManager.Languages.First(x => x.Name == value);
                _translationManager.CurrentLanguage = lang;
            }
        }
    }

    private int _timeToShowPopupSec;
    public int TimeToShowPopupSec
    {
        get => _timeToShowPopupSec;
        set => SetProperty(ref _timeToShowPopupSec, value);
    }

    public int SortingColumn { get; set; }

    public ListSortDirection SortingDirection { get; set; }

    public ICommand SaveCommand { get; }

    public SettingsViewModel(IRegionManager regionManager, IContainerProvider container, IMapper mapper,
        TranslateManager translationManager, SettingsService settingsService, MenuService menuService)
    : base(container)
    {
        _regionManager = regionManager;
        _translationManager = translationManager;
        _settingsService = settingsService;
        _mapper = mapper;
        _menuService = menuService;

        SaveCommand = new DelegateCommand(OkExecute);
    }

    public override void OnNavigatedTo(NavigationContext navigationContext)
    {
        _menuService.MainTitleId = "SettingsViewTitle";
    }

    private async void OkExecute()
    {
        var settings = _mapper.Map<SettingsModel>(this);
        await _settingsService.SaveAsync(settings);
        _regionManager.RequestNavigate(RegionConstants.Content, nameof(TargetListView));
    }
}