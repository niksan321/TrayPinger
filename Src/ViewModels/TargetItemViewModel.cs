﻿using System.ComponentModel;
using System.Windows.Input;
using Prism.Commands;
using Prism.Ioc;
using Prism.Regions;
using Prism.Services.Dialogs;
using TrayPinger.Constants;
using TrayPinger.Enums;
using TrayPinger.Models;
using TrayPinger.Services;
using TrayPinger.Views;
using Wpf.Tr;

namespace TrayPinger.ViewModels;

public class TargetItemViewModel : BaseViewModel, IRequestTarget
{
    private RequestType _type;
    public RequestType Type
    {
        get => _type;
        set => SetProperty(ref _type, value);
    }

    private string _ipOrAddress;
    public string IpOrAddress
    {
        get => _ipOrAddress;
        set => SetProperty(ref _ipOrAddress, value);
    }

    private string _postPayload;
    public string PostPayload
    {
        get => _postPayload;
        set => SetProperty(ref _postPayload, value);
    }

    private string _name;
    public string Name
    {
        get => _name;
        set => SetProperty(ref _name, value);
    }

    private TimePeriod _requestTimeout;
    public TimePeriod RequestTimeout
    {
        get => _requestTimeout;
        set
        {
            if (SetProperty(ref _requestTimeout, value))
            {
                _requestTimeout.PropertyChanged -= RequestTimeoutOnPropertyChanged;
                value.PropertyChanged += RequestTimeoutOnPropertyChanged;
            }
        }
    }

    private TimePeriod _requestInterval;
    public TimePeriod RequestInterval
    {
        get => _requestInterval;
        set
        {
            if (SetProperty(ref _requestInterval, value))
            {
                _requestInterval.PropertyChanged -= RequestIntervalOnPropertyChanged;
                value.PropertyChanged += RequestIntervalOnPropertyChanged;
            }

        }
    }

    /// <summary>
    /// Количество FailsCount чтобы сделать IsSuccess = false
    /// </summary>
    private int _countToFail;
    public int CountToFail
    {
        get => _countToFail;
        set => SetProperty(ref _countToFail, value);
    }

    private int _failsCount;
    public int FailsCount
    {
        get => _failsCount;
        set => SetProperty(ref _failsCount, value);
    }

    private bool _isSuccess = true;
    public bool IsSuccess
    {
        get => _isSuccess;
        set => SetProperty(ref _isSuccess, value);
    }

    private bool _enabled;
    public bool Enabled
    {
        get => _enabled;
        set => SetProperty(ref _enabled, value);
    }

    private bool _isRunning;
    public bool IsRunning
    {
        get => _isRunning;
        set => SetProperty(ref _isRunning, value);
    }

    private bool _isRequestSending;
    public bool IsRequestSending
    {
        get => _isRequestSending;
        set => SetProperty(ref _isRequestSending, value);
    }

    private TimeSpan _lastResponseTimeMs;
    public TimeSpan LastResponseTimeMs
    {
        get => _lastResponseTimeMs;
        set => SetProperty(ref _lastResponseTimeMs, value);
    }

    private string _message;
    public string Message
    {
        get => _message;
        set => SetProperty(ref _message, value);
    }

    public Guid Id { get; set; }
    public RequestScheduleTask ScheduleTask { get; set; }

    public ICommand EditCommand { get; }
    public ICommand DeleteCommand { get; }

    public TimePeriod MinRequestInterval => new(TimePeriodType.Milliseconds, 50);
    public TimePeriod MinRequestTimeOut => new(TimePeriodType.Milliseconds, 1);

    private readonly IRegionManager _regionManager;
    private readonly IDialogService _dialogService;
    private readonly TargetItemStorageService _storageService;
    private readonly SettingsService _settingsService;
    private readonly TranslateManager _translationManager;

    public TargetItemViewModel(IRegionManager regionManager, IContainerProvider container, IDialogService dialogService,
            TargetItemStorageService storageService, SettingsService settingsService, TranslateManager translationManager)
        : base(container)
    {
        _regionManager = regionManager;
        _dialogService = dialogService;
        _storageService = storageService;
        _settingsService = settingsService;
        _translationManager = translationManager;

        DeleteCommand = new DelegateCommand(DeleteTarget);
        EditCommand = new DelegateCommand(EditTarget);
    }

    public void Reset()
    {
        Message = null;
        FailsCount = 0;
        IsRequestSending = false;
        IsSuccess = true;
        IsRunning = false;
        LastResponseTimeMs = TimeSpan.Zero;
    }

    private void RequestIntervalOnPropertyChanged(object sender, PropertyChangedEventArgs e)
    {
        RaisePropertyChanged(nameof(RequestInterval));
    }

    //todo убрать в BindableBaseEx
    private void RequestTimeoutOnPropertyChanged(object sender, PropertyChangedEventArgs e)
    {
        RaisePropertyChanged(nameof(RequestTimeout));
    }

    private void DeleteTarget()
    {
        if (_settingsService.Current.ShowDeletePrompt)
        {
            DeleteTargetWithPrompt();
        }
        else
        {
            _storageService.Remove(this);
        }
    }

    private void DeleteTargetWithPrompt()
    {
        var parameters = new DialogParameters
        {
            { "Title", _translationManager.Translate(nameof(Langs.Langs.Warning)) },
            { "Text", _translationManager.Translate(nameof(Langs.Langs.DeleteDialogText), Name) },
        };

        _dialogService.ShowDialog(nameof(YesNoDialogView), parameters, result =>
        {
            if (result.Result == ButtonResult.Yes)
            {
                _storageService.Remove(this);
            }
        });
    }

    private void EditTarget()
    {
        var parameters = new NavigationParameters { { nameof(TargetItemViewModel), this } };
        _regionManager.RequestNavigate(RegionConstants.Content, nameof(EditTargetView), parameters);
    }
}