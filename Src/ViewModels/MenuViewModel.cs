﻿using Prism.Commands;
using Prism.Ioc;
using Prism.Regions;
using TrayPinger.Constants;
using TrayPinger.Managers;
using TrayPinger.Services;
using TrayPinger.Views;

namespace TrayPinger.ViewModels;

public class MenuViewModel : BaseViewModel
{
    private readonly IRegionManager _regionManager;
    public WindowManager WindowManager { get; }
    public MenuService MenuService { get; }

    public DelegateCommand CloseCommand { get; }
    public DelegateCommand MaximizeOrDefaultCommand { get; set; }
    public DelegateCommand SettingsCommand { get; }

    public MenuViewModel(IRegionManager regionManager, IContainerProvider container,
        WindowManager windowManager, MenuService menuService, TrayService trayService)
        : base(container)
    {
        _regionManager = regionManager;

        WindowManager = windowManager;
        MenuService = menuService;
        CloseCommand = new DelegateCommand(trayService.Hide);
        SettingsCommand = new DelegateCommand(OpenSettings);
        MaximizeOrDefaultCommand = new DelegateCommand(windowManager.MaximizeOrDefault);
    }

    private void OpenSettings()
    {
        _regionManager.RequestNavigate(RegionConstants.Content, nameof(SettingsView));
    }
}