﻿using Prism.Ioc;
using Prism.Mvvm;
using Prism.Regions;

namespace TrayPinger.ViewModels;

public abstract class BaseViewModel : BindableBase, INavigationAware, IRegionMemberLifetime
{
    public IContainerProvider Container { get; }
    public bool KeepAlive => Container.CurrentScope.IsAttached;

    public BaseViewModel(IContainerProvider container)
    {
        Container = container;
    }

    public virtual void OnNavigatedTo(NavigationContext navigationContext) { }

    public bool IsNavigationTarget(NavigationContext navigationContext)
    {
        return true;
    }

    public void OnNavigatedFrom(NavigationContext navigationContext)
    {
    }
}