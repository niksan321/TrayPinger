﻿using System.ComponentModel;
using MapsterMapper;
using TrayPinger.Enums;
using TrayPinger.Models;
using TrayPinger.ViewModels;
using Wpf.Tr;

namespace TrayPinger.Helpers;

public class DataSeedHelper
{
    private readonly IMapper _mapper;
    private readonly TranslateManager _translationManager;

    private static string DefaultPostPayload =>
        @"{
          ""id"": 102,
          ""category"": {
            ""id"": 1,
            ""name"": ""cat""
    }}";

    public DataSeedHelper(IMapper mapper, TranslateManager translationManager)
    {
        _mapper = mapper;
        _translationManager = translationManager;
    }

    public SettingsModel GetDefaultSettings()
    {
        return new SettingsModel
        {
            AutoStartPing = true,
            RunOnStartUp = false,
            Notify = true,
            StartMinimized = true,
            ShowDeletePrompt = true,
            PlayErrorSound = true,
            SortingColumn = 2,
            SortingDirection = ListSortDirection.Ascending,
            TimeToShowPopupSec = 5,
            Language = _translationManager.GetSystemOrDefaultLanguage().Name,
            TargetItems = new List<TargetItemModel>(GetDefaultTargetItemModels())
        };
    }

    public TargetItemModel[] GetDefaultTargetItemModels() => new[]
        {
            new TargetItemModel
            {
                Id = Guid.NewGuid(),
                Name = _translationManager.Translate(nameof(Langs.Langs.InternetPing)),
                CountToFail = 3,
                Type = RequestType.IcmpPing,
                IpOrAddress = "ya.ru",
                Enabled = true,
                RequestTimeout = new TimePeriod(TimePeriodType.Seconds, 2),
                RequestInterval = new TimePeriod(TimePeriodType.Seconds, 2),
            },
            new TargetItemModel
            {
                Id = Guid.NewGuid(),
                Name = _translationManager.Translate(nameof(Langs.Langs.InternetGet)),
                CountToFail = 3,
                Type = RequestType.HttpGet,
                IpOrAddress = "http://ya.ru",
                Enabled = true,
                RequestTimeout = new TimePeriod(TimePeriodType.Seconds, 2),
                RequestInterval = new TimePeriod(TimePeriodType.Seconds, 2),
            },
            new TargetItemModel
            {
                Id = Guid.NewGuid(),
                Name = _translationManager.Translate(nameof(Langs.Langs.InternetPost)),
                CountToFail = 3,
                Type = RequestType.HttpPost,
                IpOrAddress = "https://httpbin.org/post",
                PostPayload = DefaultPostPayload,
                Enabled = true,
                RequestTimeout = new TimePeriod(TimePeriodType.Seconds, 2),
                RequestInterval = new TimePeriod(TimePeriodType.Seconds, 2),
            }
        };

    public TargetItemModel GetDefaultTargetItemModel()
    {
        return new TargetItemModel
        {
            Id = Guid.NewGuid(),
            Name = _translationManager.Translate(nameof(Langs.Langs.InternetPing)),
            CountToFail = 3,
            Type = RequestType.IcmpPing,
            IpOrAddress = "ya.ru",
            Enabled = true,
            RequestTimeout = new TimePeriod(TimePeriodType.Seconds, 2),
            RequestInterval = new TimePeriod(TimePeriodType.Seconds, 2),
        };
    }

    public TargetItemViewModel GetDefaultTargetItemViewModel(int index)
    {
        var defaultModel = GetDefaultTargetItemModel();
        defaultModel.Name = _translationManager.Translate(nameof(Langs.Langs.NewTargetName), index);
        var defaultViewModel = _mapper.Map<TargetItemViewModel>(defaultModel);

        return defaultViewModel;
    }
}