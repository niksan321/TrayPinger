﻿using System.Diagnostics;
using MaterialDesignThemes.Wpf;

namespace TrayPinger.Helpers;

public static class DialogHostHelper
{
    static readonly TimeSpan _waitTimeOut = TimeSpan.FromSeconds(15);

    public static async Task<object> Show(object content)
    {
        var stopWatch = Stopwatch.StartNew();
        while (DialogHost.IsDialogOpen(null))
        {
            await Task.Delay(50);

            if (stopWatch.Elapsed > _waitTimeOut)
            {
                throw new TimeoutException("Таймаут ожидания закрытия диалогового окна");
            }
        }

        return await DialogHost.Show(content);
    }

    /// <summary>
    /// If dialog not opened and we close it - crash
    /// This may occur when we fast show/close dialogs 
    /// </summary>
    public static void Close(object parameter = null)
    {
        if (DialogHost.IsDialogOpen(null))
        {
            DialogHost.Close(null, parameter);
        }
    }
}