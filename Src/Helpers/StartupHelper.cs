﻿using System.Configuration;
using Microsoft.Win32;
using TrayPinger.Constants;

namespace TrayPinger.Helpers;

public static class StartupHelper
{
    public static void SetStartup(bool runOnStartUp)
    {
        var registryPath = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run";

        var registryKey = Registry.CurrentUser.OpenSubKey(registryPath, true)
                          ?? throw new ConfigurationErrorsException($"Не удаётся прочитать путь реестра: {registryPath}");

        if (runOnStartUp)
            registryKey.SetValue(AppConstants.Application.Name, Environment.ProcessPath!);
        else
            registryKey.DeleteValue(AppConstants.Application.Name, false);

    }
}