﻿using System.Reflection;
using System.Windows;
using Serilog;
using TrayPinger.ViewModels;
using TrayPinger.Views;

namespace TrayPinger.Helpers;

public static class ExceptionsHelper
{
    public static void SetupExceptionHandling(Application app)
    {
        AppDomain.CurrentDomain.UnhandledException += (s, e) =>
            LogUnhandledException((Exception)e.ExceptionObject, "AppDomain.CurrentDomain.UnhandledException");

        app.DispatcherUnhandledException += (s, e) =>
        {
            LogUnhandledException(e.Exception, "Application.Current.DispatcherUnhandledException");
            e.Handled = true;
        };

        TaskScheduler.UnobservedTaskException += (s, e) =>
        {
            LogUnhandledException(e.Exception, "TaskScheduler.UnobservedTaskException");
            e.SetObserved();
        };
    }

    private static async void LogUnhandledException(Exception exception, string source)
    {
        var assemblyName = Assembly.GetExecutingAssembly().GetName();
        var message = $"Не перехваченное исключение source: {source}, assembly: {assemblyName.Name}, version: {assemblyName.Version}";

        Log.Logger.Error(message);
        Log.Logger.Error(exception.Message);

        try
        {
            var model = new ExceptionViewDialogViewModel
            {
                Message = exception.Message
            };
            await DialogHostHelper.Show(new ExceptionDialogView(model));
        }
        catch (Exception ex)
        {
            Log.Logger.Error(ex, $"Исключение в методе {nameof(LogUnhandledException)}");
        }
    }
}