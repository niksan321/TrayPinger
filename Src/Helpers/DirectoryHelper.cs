﻿using System.IO;
using TrayPinger.Constants;

namespace TrayPinger.Helpers;

public static class DirectoryHelper
{
    public static string GetAppFolder()
    {
        return Path.GetDirectoryName(Environment.ProcessPath);
    }

    public static string GetLogFullPath()
    {
        var fullPath = Path.Combine(GetAppFolder(), AppConstants.Log.FileName);
        return fullPath;
    }

    public static string GetSettingsFullPath()
    {
        var fullPath = Path.Combine(GetAppFolder(), AppConstants.Settings.FileName);
        return fullPath;
    }
}