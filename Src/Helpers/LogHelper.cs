﻿using Serilog;

namespace TrayPinger.Helpers;

public static class LogHelper
{
    public static void CreateLogger()
    {
        var logFullPath = DirectoryHelper.GetLogFullPath();
        var logConfig = new LoggerConfiguration()
            .MinimumLevel.Debug()
            .WriteTo.File(logFullPath, fileSizeLimitBytes: 1_000_000, rollOnFileSizeLimit: true,
                retainedFileCountLimit: 3, shared: true);

        Log.Logger = logConfig.CreateLogger();
        Log.Logger.Information("Приложение запущено.");
    }
}