﻿using System.Windows;

namespace TrayPinger.Extensions;

public static class UiEx
{
    public static void Run(Action run)
    {
        var dispatcher = Application.Current?.Dispatcher;
        if (dispatcher == null) { return; }

        if (!dispatcher.CheckAccess())
        {
            dispatcher.BeginInvoke(run);
        }
        else
        {
            run();
        }
    }

    public static async Task RunAsync(Action run)
    {
        var dispatcher = Application.Current?.Dispatcher;
        if (dispatcher == null) { return; }

        if (!dispatcher.CheckAccess())
        {
            await dispatcher.BeginInvoke(run);
        }
        else
        {
            run();
        }
    }
}