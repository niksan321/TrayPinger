﻿using Prism.Regions;

namespace TrayPinger.Extensions;

public static class RegionManagerExtensions
{
    public static void ClearRegions(this IRegionManager regionManager)
    {
        var names = regionManager.Regions.Select(x => x.Name).ToArray();
        foreach (var name in names)
        {
            regionManager.Regions.Remove(name);
        }
    }
}