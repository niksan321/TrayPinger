﻿using System.Windows;
using System.Windows.Shell;

namespace TrayPinger.Extensions;

public static class WindowExtensions
{
    public static void RemoveBorder(this Window window)
    {
        window.SetValue(WindowChrome.WindowChromeProperty,
            new WindowChrome
            {
                CaptionHeight = 0,
                ResizeBorderThickness = new Thickness(5)
            });
    }

    public static void SetDefaultBorder(this Window window)
    {
        window.SetValue(WindowChrome.WindowChromeProperty, null);
    }
}