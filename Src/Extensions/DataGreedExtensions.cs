﻿using System.ComponentModel;
using System.Windows.Controls;

namespace TrayPinger.Extensions;

public static class DataGreedExtensions
{
    public static void SortDataGrid(this DataGrid dataGrid, int columnIndex = 0,
        ListSortDirection sortDirection = ListSortDirection.Ascending)
    {
        var column = dataGrid.Columns[columnIndex];
        if (column is not { CanUserSort: true })
        {
            column = dataGrid.Columns.First(x => x.CanUserSort);
        }

        dataGrid.Items.SortDescriptions.Clear();
        var sortDescription = new SortDescription(column.SortMemberPath, sortDirection);
        dataGrid.Items.SortDescriptions.Add(sortDescription);

        foreach (var col in dataGrid.Columns)
        {
            col.SortDirection = null;
        }

        column.SortDirection = sortDirection;
        dataGrid.Items.Refresh();
    }

    public static void SortDataGrid(this DataGrid dataGrid, string columnName = "",
        ListSortDirection sortDirection = ListSortDirection.Ascending)
    {
        var column = dataGrid.Columns.FirstOrDefault(x => x.Header.ToString() == columnName);
        SortDataGrid(dataGrid, column == null ? 0 : dataGrid.Columns.IndexOf(column));
    }
}