﻿using System.Diagnostics;
using System.Net.Http;

namespace TrayPinger.Extensions;

public static class HttpClientExtensions
{
    public static async Task<Tuple<HttpResponseMessage, TimeSpan>> GetWithElapsedAsync(this HttpClient client, string url)
    {
        var stopWatch = Stopwatch.StartNew();
        using var result = await client.GetAsync(url);
        return new Tuple<HttpResponseMessage, TimeSpan>(result, stopWatch.Elapsed);
    }

    public static async Task<Tuple<HttpResponseMessage, TimeSpan>> PostWithElapsedAsync(this HttpClient client, string url, string content)
    {
        var stopWatch = Stopwatch.StartNew();
        using var result = await client.PostAsync(url, new StringContent(content));
        return new Tuple<HttpResponseMessage, TimeSpan>(result, stopWatch.Elapsed);
    }
}