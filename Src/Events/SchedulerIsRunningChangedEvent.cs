﻿using Prism.Events;

namespace TrayPinger.Events;

public class SchedulerIsRunningChangedEvent : PubSubEvent<bool> { }