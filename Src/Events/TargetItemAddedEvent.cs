﻿using Prism.Events;
using TrayPinger.ViewModels;

namespace TrayPinger.Events;

public class TargetItemAddedEvent : PubSubEvent<TargetItemViewModel> { }