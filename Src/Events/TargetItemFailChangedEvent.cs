﻿using Prism.Events;

namespace TrayPinger.Events;

public class TargetItemFailChangedEvent : PubSubEvent { }