﻿using Prism.Events;
using TrayPinger.Models;

namespace TrayPinger.Events;

public class SettingsLoadedEvent : PubSubEvent<SettingsModel> { }