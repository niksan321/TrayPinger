﻿using Prism.Events;
using TrayPinger.ViewModels;

namespace TrayPinger.Events;

public class TargetItemRemovedEvent : PubSubEvent<TargetItemViewModel> { }