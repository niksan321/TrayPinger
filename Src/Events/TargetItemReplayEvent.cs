﻿using Prism.Events;
using TrayPinger.Models;

namespace TrayPinger.Events;

public class TargetItemReplayEvent : PubSubEvent<TargetItemReplayEventPayload> { }