﻿using Prism.Events;

namespace TrayPinger.Events;

public class ApplicationFirstRunEvent : PubSubEvent { }