﻿using Prism.Events;
using TrayPinger.ViewModels;

namespace TrayPinger.Events;

public class TargetItemChangedEvent : PubSubEvent<TargetItemViewModel> { }