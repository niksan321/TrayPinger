﻿namespace TrayPinger.Models;

public interface IRequestTarget
{
    string IpOrAddress { get; }

    TimePeriod RequestTimeout { get; }

    TimePeriod RequestInterval { get; }

    public string PostPayload { get; }
}