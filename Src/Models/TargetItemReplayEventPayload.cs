﻿using TrayPinger.ViewModels;

namespace TrayPinger.Models;

public record TargetItemReplayEventPayload(TargetItemViewModel Target, RequestReplay Replay) { }