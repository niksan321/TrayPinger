﻿using TrayPinger.Enums;

namespace TrayPinger.Models;

public class TargetItemModel
{
    public Guid Id { get; set; }

    public RequestType Type { get; set; }

    public string IpOrAddress { get; set; }

    public string PostPayload { get; set; }

    public string Name { get; set; }

    public TimePeriod RequestTimeout { get; set; }

    public TimePeriod RequestInterval { get; set; }

    /// <summary>
    /// FailsCount to set IsSuccess = false
    /// </summary>
    public int CountToFail { get; set; }

    public bool Enabled { get; set; }
}