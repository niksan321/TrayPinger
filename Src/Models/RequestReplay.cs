﻿namespace TrayPinger.Models;

public class RequestReplay
{
    public bool IsSended { get; set; }
    public bool IsSuccess { get; set; }
    public string Message { get; set; }
    public int RoundtripTimeMs { get; set; }

    public static RequestReplay Sending { get; }

    static RequestReplay()
    {
        Sending = new RequestReplay
        {
            IsSended = false,
            Message = "Sending",
            IsSuccess = false,
            RoundtripTimeMs = 0
        };
    }

    public static RequestReplay Error(Exception ex)
    {
        return new RequestReplay
        {
            IsSuccess = false,
            IsSended = true,
            Message = ex.InnerException?.Message ?? ex.Message
        };
    }
}