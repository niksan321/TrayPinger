﻿using Prism.Mvvm;
using TrayPinger.Enums;

namespace TrayPinger.Models;

public class TimePeriod : BindableBase
{
    private int _value;
    public int Value
    {
        get => _value;
        set => SetProperty(ref _value, value);
    }

    private TimePeriodType _type;
    public TimePeriodType Type
    {
        get => _type;
        set => SetProperty(ref _type, value);
    }

    public TimePeriod(TimePeriodType type, int value)
    {
        _type = type;
        _value = value;
    }

    public static bool operator >(TimePeriod a, TimePeriod b)
    {
        if (a == null && b == null) return false;
        if (a == null) return false;
        if (b == null) return true;

        if (a.Type != b.Type) return a.Type > b.Type;

        if (a.Type == b.Type)
        {
            return a.Value > b.Value;
        }

        return false;
    }

    public static bool operator <(TimePeriod a, TimePeriod b)
    {
        if (a == null && b == null) return false;
        if (a == null) return true;
        if (b == null) return false;

        if (a.Type != b.Type) return a.Type < b.Type;

        if (a.Type == b.Type)
        {
            return a.Value < b.Value;
        }

        return false;
    }

    public override string ToString()
    {
        return $"{Value} {Type}";
    }

    public TimeSpan ToTimeSpan()
    {
        return Type switch
        {
            TimePeriodType.Milliseconds => TimeSpan.FromMilliseconds(Value),
            TimePeriodType.Seconds => TimeSpan.FromSeconds(Value),
            TimePeriodType.Hours => TimeSpan.FromHours(Value),
            TimePeriodType.Days => TimeSpan.FromDays(Value),
            _ => throw new ArgumentOutOfRangeException(nameof(Type))
        };
    }
}