﻿using System.ComponentModel;

namespace TrayPinger.Models;

public class SettingsModel
{
    public List<TargetItemModel> TargetItems { get; set; }

    public bool RunOnStartUp { get; set; }

    public bool AutoStartPing { get; set; }

    public bool Notify { get; set; }

    public bool StartMinimized { get; set; }

    public bool ShowDeletePrompt { get; set; }

    public bool PlayErrorSound { get; set; }

    public string Language { get; set; }

    public int TimeToShowPopupSec { get; set; }

    public int SortingColumn { get; set; }

    public ListSortDirection SortingDirection { get; set; }
}