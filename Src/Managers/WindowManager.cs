﻿using System.Windows;
using System.Windows.Input;
using Prism.Mvvm;
using TrayPinger.Extensions;

namespace TrayPinger.Managers;

public class WindowManager : BindableBase, IDisposable
{
    private static Window MainWindow => Application.Current.MainWindow;
    private bool _restoreDragMove;

    private bool _isMaximized;
    public bool IsMaximized
    {
        get => _isMaximized;
        set => SetProperty(ref _isMaximized, value);
    }

    public WindowManager()
    {
        SubscribeForEvents();
    }

    private void SubscribeForEvents()
    {
        MainWindow.StateChanged += OnMainWindow_StateChanged;
    }

    private void UnSubscribeForEvents()
    {
        MainWindow.StateChanged -= OnMainWindow_StateChanged;
    }

    private void OnMainWindow_StateChanged(object sender, EventArgs e)
    {
        switch (MainWindow.WindowState)
        {
            case WindowState.Normal:
                MainWindow.RemoveBorder();
                IsMaximized = false;
                break;
            case WindowState.Minimized:
                IsMaximized = false;
                break;
            case WindowState.Maximized:
                IsMaximized = true;
                MainWindow.SetDefaultBorder();
                break;
            default:
                throw new ArgumentOutOfRangeException($"Не поддерживаемое состояние: {MainWindow.WindowState}");
        }
    }

    public void OnHeaderMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
        if (e.ClickCount == 2)
        {
            if (MainWindow.ResizeMode != ResizeMode.CanResize &&
                MainWindow.ResizeMode != ResizeMode.CanResizeWithGrip)
            {
                return;
            }

            MaximizeOrDefault();
        }
        else
        {
            _restoreDragMove = MainWindow.WindowState == WindowState.Maximized;
            MainWindow.DragMove();
        }
    }

    public void OnHeaderMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
    {
        _restoreDragMove = false;
    }

    public void OnHeaderMouseMove(object sender, MouseEventArgs e)
    {
        if (!_restoreDragMove) return;

        _restoreDragMove = false;
        var point = MainWindow.PointToScreen(e.MouseDevice.GetPosition(MainWindow));

        MainWindow.Left = point.X - (MainWindow.RestoreBounds.Width * 0.5);
        MainWindow.Top = point.Y;

        MainWindow.WindowState = WindowState.Normal;
        MainWindow.DragMove();
    }

    public void MaximizeOrDefault()
    {
        MainWindow.WindowState = IsMaximized ? WindowState.Normal : WindowState.Maximized;
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        UnSubscribeForEvents();
    }
}