﻿using Mapster;
using TrayPinger.Models;
using TrayPinger.ViewModels;

namespace TrayPinger.Mappings;

public class MappingRegistrations : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<SettingsModel, SettingsViewModel>()
            .MapToConstructor(true)
            .ConstructUsing(x => MapContext.Current.GetService<SettingsViewModel>());

        config.NewConfig<TargetItemModel, TargetItemViewModel>()
            .MapToConstructor(true)
            .ConstructUsing(x => MapContext.Current.GetService<TargetItemViewModel>());
    }
}