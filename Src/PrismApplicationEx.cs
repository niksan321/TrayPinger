﻿using Prism.DryIoc;
using Prism.Ioc;
using Prism.Regions;
using TrayPinger.Constants;
using TrayPinger.Views;

namespace TrayPinger;

public abstract class PrismApplicationEx : PrismApplication
{
    public void Start()
    {
        var regionManager = Container.Resolve<IRegionManager>();
        var shell = Container.Resolve<ShallView>();
        RegionManager.SetRegionManager(shell, regionManager);

        MainWindow = shell;
        MainWindow!.Show();

        regionManager.RequestNavigate(RegionConstants.Content, nameof(TargetListView));
    }
}