﻿using System.ComponentModel;
using System.Windows.Controls;
using Prism.Ioc;
using TrayPinger.Extensions;
using TrayPinger.Services;
using TrayPinger.ViewModels;

namespace TrayPinger.Views;

/// <summary>
/// Interaction logic for TargetListPage.xaml
/// </summary>
public partial class TargetListView : UserControl
{
    private readonly SettingsService _settingsService;
    private readonly IContainerProvider _container;
    private TargetListViewModel Model => DataContext as TargetListViewModel;

    public TargetListView(SettingsService settingsService, IContainerProvider container)
    {
        _settingsService = settingsService;
        _container = container;
        InitializeComponent();
        TargetListDataGrid.Loaded += TargetListDataGrid_Loaded;
        TargetListDataGrid.Sorting += TargetListDataGrid_Sorting;

        var currentSettings = settingsService.Current;
        TargetListDataGrid.SortDataGrid(currentSettings.SortingColumn, currentSettings.SortingDirection);
    }

    private async void TargetListDataGrid_Sorting(object sender, DataGridSortingEventArgs e)
    {
        var oldSortDirection = e.Column.SortDirection;
        var newSortDirection = oldSortDirection == ListSortDirection.Ascending
            ? ListSortDirection.Descending
            : ListSortDirection.Ascending;

        var currentSettings = _settingsService.Current;
        currentSettings.SortingDirection = newSortDirection;
        currentSettings.SortingColumn = e.Column.DisplayIndex;

        await _settingsService.SaveAsync();
    }

    private void TargetListDataGrid_Loaded(object sender, System.Windows.RoutedEventArgs e)
    {
        FixFirstCellStyleError();
        TargetListDataGrid.Loaded -= TargetListDataGrid_Loaded;
    }

    private void FixFirstCellStyleError()
    {
        var item = _container.Resolve<TargetItemViewModel>();
        Model.Targets.Add(item);

        TargetListDataGrid.UpdateLayout();
        TargetListDataGrid.Items.Refresh();

        Model.Targets.Remove(item);
    }
}