﻿using System.Windows.Controls;
using TrayPinger.ViewModels;

namespace TrayPinger.Views;

/// <summary>
/// Interaction logic for YesCancelView.xaml
/// </summary>
public partial class ExceptionDialogView : UserControl
{
    public ExceptionDialogView(ExceptionViewDialogViewModel dc)
    {
        DataContext = dc;
        InitializeComponent();
    }
}