﻿using System.Windows.Controls;
using System.Windows.Input;
using TrayPinger.Managers;

namespace TrayPinger.Views;

/// <summary>
/// Interaction logic for HomeMenuComponent.xaml
/// </summary>
public partial class MenuView : UserControl
{
    private readonly WindowManager _manager;

    public MenuView(WindowManager manager)
    {
        _manager = manager;
        InitializeComponent();
    }

    private void UIElement_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
        _manager.OnHeaderMouseLeftButtonDown(sender, e);
    }

    private void UIElement_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
    {
        _manager.OnHeaderMouseLeftButtonUp(sender, e);
    }

    private void UIElement_OnMouseMove(object sender, MouseEventArgs e)
    {
        _manager.OnHeaderMouseMove(sender, e);
    }
}