﻿using System.Windows;
using TrayPinger.Extensions;

namespace TrayPinger.Views;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class ShallView : Window
{
    public ShallView()
    {
        this.RemoveBorder();
        InitializeComponent();
    }
}