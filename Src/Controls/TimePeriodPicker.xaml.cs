﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Prism.Commands;
using TrayPinger.Enums;
using TrayPinger.Models;

namespace TrayPinger.Controls;

public partial class TimePeriodPicker : UserControl
{
    public static readonly DependencyProperty ValueProperty =
        DependencyProperty.Register(nameof(Value), typeof(int), typeof(TimePeriodPicker),
            new UIPropertyMetadata(10, null, CoerceValueCallback));

    private static object CoerceValueCallback(DependencyObject d, object value)
    {
        var newValue = (int)value;
        var picker = (TimePeriodPicker)d;

        if (newValue < picker.Min.Value && picker.Type == picker.Min.Type)
        {
            return picker.Min.Value;
        }

        if (newValue > picker.Max.Value && picker.Type == picker.Max.Type)
        {
            return picker.Max.Value;
        }

        if (newValue <= 0)
        {
            return 1;
        }

        return newValue;
    }

    public int Value
    {
        get => (int)GetValue(ValueProperty);
        set => SetValue(ValueProperty, value);
    }

    public static readonly DependencyProperty TypeProperty =
        DependencyProperty.Register(nameof(Type), typeof(TimePeriodType), typeof(TimePeriodPicker),
            new UIPropertyMetadata(TimePeriodType.Seconds, TypeChanged, CoerceType));

    private static void TypeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        d.CoerceValue(ValueProperty);
    }

    public TimePeriodType Type
    {
        get => (TimePeriodType)GetValue(TypeProperty);
        set => SetValue(TypeProperty, value);
    }

    private static object CoerceType(DependencyObject d, object value)
    {
        var picker = (TimePeriodPicker)d;
        var newType = (TimePeriodType)value;

        if (picker.Max.Type < newType)
        {
            return picker.Max.Type;
        }

        if (picker.Min.Type > newType)
        {
            return picker.Min.Type;
        }

        return newType;
    }

    public static readonly DependencyProperty MaxProperty =
        DependencyProperty.Register(nameof(Max), typeof(TimePeriod), typeof(TimePeriodPicker),
            new UIPropertyMetadata(new TimePeriod(TimePeriodType.Days, 1)));

    public TimePeriod Max
    {
        get => (TimePeriod)GetValue(MaxProperty);
        set => SetValue(MaxProperty, value);
    }

    public static readonly DependencyProperty MinProperty =
        DependencyProperty.Register(nameof(Min), typeof(TimePeriod), typeof(TimePeriodPicker),
            new UIPropertyMetadata(new TimePeriod(TimePeriodType.Milliseconds, 100)));

    public TimePeriod Min
    {
        get => (TimePeriod)GetValue(MinProperty);
        set => SetValue(MinProperty, value);
    }

    private ICommand _increaseCommand;
    public ICommand IncreaseCommand => _increaseCommand ??= new DelegateCommand(IncreaseValue);

    private ICommand _decreaseCommand;
    public ICommand DecreaseCommand => _decreaseCommand ??= new DelegateCommand(DecreaseValue);

    public TimePeriodPicker()
    {
        InitializeComponent();
    }

    private void DecreaseValue()
    {
        Value--;
    }

    private void IncreaseValue()
    {
        Value++;
    }
}