﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Prism.Commands;

namespace TrayPinger.Controls;

public partial class NumericUpDown : UserControl
{
    public static readonly DependencyProperty ValueProperty =
        DependencyProperty.Register(nameof(Value), typeof(int), typeof(NumericUpDown),
            new FrameworkPropertyMetadata(10, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, null, CoerceValueCallback));

    private static object CoerceValueCallback(DependencyObject d, object value)
    {
        var newValue = (int)value;
        var picker = (NumericUpDown)d;

        if (newValue < picker.Min)
        {
            return picker.Min;
        }

        if (newValue > picker.Max)
        {
            return picker.Max;
        }

        return newValue;
    }

    public int Value
    {
        get => (int)GetValue(ValueProperty);
        set => SetValue(ValueProperty, value);
    }

    public static readonly DependencyProperty IncrementProperty =
        DependencyProperty.Register(nameof(Increment), typeof(int), typeof(NumericUpDown),
            new UIPropertyMetadata(1));

    public int Increment
    {
        get => (int)GetValue(MaxProperty);
        set => SetValue(MaxProperty, value);
    }

    public static readonly DependencyProperty MaxProperty =
        DependencyProperty.Register(nameof(Max), typeof(int), typeof(NumericUpDown),
            new UIPropertyMetadata(int.MaxValue));

    public int Max
    {
        get => (int)GetValue(MaxProperty);
        set => SetValue(MaxProperty, value);
    }

    public static readonly DependencyProperty MinProperty =
        DependencyProperty.Register(nameof(Min), typeof(int), typeof(NumericUpDown),
            new UIPropertyMetadata(int.MinValue));

    public int Min
    {
        get => (int)GetValue(MinProperty);
        set => SetValue(MinProperty, value);
    }

    public ICommand IncreaseCommand { get; }

    public ICommand DecreaseCommand { get; }

    public NumericUpDown()
    {
        InitializeComponent();
        DecreaseCommand = new DelegateCommand(DecreaseValue);
        IncreaseCommand = new DelegateCommand(IncreaseValue);
    }

    private void DecreaseValue()
    {
        Value--;
    }

    private void IncreaseValue()
    {
        Value++;
    }
}