﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Hardcodet.Wpf.TaskbarNotification;
using Prism.Commands;
using TrayPinger.Enums;

namespace TrayPinger.Controls;

public partial class MessageBalloon : UserControl
{
    private bool _isClosing;

    public MessageType MessageType { get; }
    public string Text { get; }
    public DelegateCommand CloseCommand { get; }

    public MessageBalloon(MessageType messageType, string text)
    {
        Text = text;
        MessageType = messageType;
        CloseCommand = new DelegateCommand(Close);

        InitializeComponent();
        TaskbarIcon.AddBalloonClosingHandler(this, OnBalloonClosing);
    }

    private void Close()
    {
        var tbIcon = TaskbarIcon.GetParentTaskbarIcon(this);
        tbIcon.CloseBalloon();
    }

    /// <summary>
    /// By subscribing to the <see cref="TaskbarIcon.BalloonClosingEvent"/>
    /// and setting the "Handled" property to true, we suppress the popup
    /// from being closed in order to display the custom fade-out animation.
    /// </summary>
    private void OnBalloonClosing(object sender, RoutedEventArgs e)
    {
        e.Handled = true; //suppresses the popup from being closed immediately
        _isClosing = true;
    }

    /// <summary>
    /// Closes the popup once the fade-out animation completed.
    /// The animation was triggered in XAML through the attached
    /// BalloonClosing event.
    /// </summary>
    private void OnFadeOutCompleted(object sender, EventArgs e)
    {
        var pp = (Popup)Parent;
        pp.IsOpen = false;
    }

    /// <summary>
    /// If the users hovers over the balloon, we don't close it.
    /// </summary>
    private void Balloon_OnMouseEnter(object sender, MouseEventArgs e)
    {
        //if we're already running the fade-out animation, do not interrupt anymore
        //(makes things too complicated for the sample)
        if (_isClosing) return;

        //the tray icon assigned this attached property to simplify access
        var tbIcon = TaskbarIcon.GetParentTaskbarIcon(this);
        tbIcon.ResetBalloonCloseTimer();
    }
}