﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using MaterialDesignThemes.Wpf;
using Microsoft.Xaml.Behaviors.Core;

namespace TrayPinger.Controls;

public class TwoStateButton : Control, INotifyPropertyChanged
{
    public static readonly DependencyProperty CommandProperty = DependencyProperty.Register(
        nameof(Command), typeof(ICommand), typeof(TwoStateButton),
        new FrameworkPropertyMetadata(default(ICommand)));

    public ICommand Command
    {
        get => (ICommand)GetValue(CommandProperty);
        set => SetValue(CommandProperty, value);
    }

    public static readonly DependencyProperty KindProperty = DependencyProperty.Register(
        nameof(Kind), typeof(PackIconKind), typeof(TwoStateButton), new PropertyMetadata(default(PackIconKind)));

    public PackIconKind Kind
    {
        get => (PackIconKind)GetValue(KindProperty);
        set => SetValue(KindProperty, value);
    }

    public static readonly DependencyProperty KindToggledProperty = DependencyProperty.Register(
        nameof(KindToggled), typeof(PackIconKind), typeof(TwoStateButton), new PropertyMetadata(default(PackIconKind), KindToggledChangedCallback));

    private static void KindToggledChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        if (d is TwoStateButton iconButton && iconButton.Kind != (PackIconKind)e.NewValue)
        {
            iconButton.CanToggle = true;
        }
    }

    public PackIconKind KindToggled
    {
        get => (PackIconKind)GetValue(KindToggledProperty);
        set => SetValue(KindToggledProperty, value);
    }

    public static readonly DependencyProperty IsToggledProperty = DependencyProperty.Register(
        nameof(IsToggled), typeof(bool), typeof(TwoStateButton),
        new FrameworkPropertyMetadata(default(bool), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));


    public bool IsToggled
    {
        get => (bool)GetValue(IsToggledProperty);
        set => SetValue(IsToggledProperty, value);
    }

    public static readonly DependencyProperty AutoToggleProperty = DependencyProperty.Register(
        nameof(AutoToggle), typeof(bool), typeof(TwoStateButton),
        new FrameworkPropertyMetadata(true, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));


    public bool AutoToggle
    {
        get => (bool)GetValue(AutoToggleProperty);
        set => SetValue(AutoToggleProperty, value);
    }

    public static readonly DependencyProperty ToolTipToggledProperty = DependencyProperty.Register(
        nameof(ToolTipToggled), typeof(object), typeof(TwoStateButton), new PropertyMetadata(default(object)));

    public object ToolTipToggled
    {
        get => GetValue(ToolTipToggledProperty);
        set => SetValue(ToolTipToggledProperty, value);
    }

    private bool _canToggle;
    public bool CanToggle
    {
        get => _canToggle;
        set => SetField(ref _canToggle, value);
    }

    private ICommand _changeToggleCommand;
    public ICommand ChangeToggleCommand
    {
        get => _changeToggleCommand;
        set => SetField(ref _changeToggleCommand, value);
    }

    static TwoStateButton()
    {
        DefaultStyleKeyProperty.OverrideMetadata(typeof(TwoStateButton),
            new FrameworkPropertyMetadata(typeof(TwoStateButton)));
    }

    public TwoStateButton()
    {
        ChangeToggleCommand = new ActionCommand(ChangeToggleExecute);
    }

    private void ChangeToggleExecute()
    {
        if (AutoToggle)
        {
            IsToggled = !IsToggled;
        }
    }

    #region INotifyPropertyChanged

    public event PropertyChangedEventHandler PropertyChanged;

    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
    {
        if (EqualityComparer<T>.Default.Equals(field, value)) return false;
        field = value;
        OnPropertyChanged(propertyName);
        return true;
    }

    #endregion
}