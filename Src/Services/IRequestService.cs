﻿using TrayPinger.Models;

namespace TrayPinger.Services;

public interface IRequestService
{
    Task<RequestReplay> SendAsync(IRequestTarget target);
}