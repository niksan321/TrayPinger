﻿using Prism.Events;
using TrayPinger.Events;
using TrayPinger.Models;
using TrayPinger.ViewModels;

namespace TrayPinger.Services;

public class TargetItemStorageService
{
    private readonly IEventAggregator _eventAggregator;
    public ObservableCollectionEx<TargetItemViewModel> Items { get; }

    public TargetItemStorageService(IEventAggregator eventAggregator)
    {
        _eventAggregator = eventAggregator;
        Items = new ObservableCollectionEx<TargetItemViewModel>();
    }

    public void Add(TargetItemViewModel target)
    {
        Items.Add(target);
        _eventAggregator.GetEvent<TargetItemAddedEvent>().Publish(target);
    }

    public void Init(IEnumerable<TargetItemViewModel> targets)
    {
        foreach (var target in targets)
        {
            Items.Add(target);
        }

        _eventAggregator.GetEvent<TargetItemsStorageInitEvent>().Publish(Items.ToArray());
    }

    public void Remove(TargetItemViewModel target)
    {
        Items.Remove(target);
        _eventAggregator.GetEvent<TargetItemRemovedEvent>().Publish(target);
    }
}