﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using TrayPinger.Events;

namespace TrayPinger.Services;

public class SchedulerService : BindableBase
{
    private readonly IEventAggregator _eventAggregator;
    private readonly List<RequestScheduleTask> _scheduleTasks = new();

    private bool _isRunning;
    public bool IsRunning
    {
        get => _isRunning;
        private set => SetProperty(ref _isRunning, value);
    }

    public DelegateCommand StartStopCommand { get; }

    public SchedulerService(IEventAggregator eventAggregator)
    {
        _eventAggregator = eventAggregator;
        StartStopCommand = new DelegateCommand(StartStop);
    }

    public void AddTask(RequestScheduleTask task)
    {
        _scheduleTasks.Add(task);
        if (IsRunning) task.Start();
    }

    public void RemoveTask(RequestScheduleTask task)
    {
        _scheduleTasks.Remove(task);
        task.Stop();
        task.Dispose();
    }

    public void Run()
    {
        foreach (var task in _scheduleTasks)
        {
            task.Start();
        }

        IsRunning = true;
        _eventAggregator.GetEvent<SchedulerIsRunningChangedEvent>().Publish(IsRunning);
    }

    public void Stop()
    {
        foreach (var task in _scheduleTasks)
        {
            task.Stop();
        }

        IsRunning = false;
        _eventAggregator.GetEvent<SchedulerIsRunningChangedEvent>().Publish(IsRunning);
    }

    public void Toggle()
    {
        if (IsRunning) Stop(); else Run();
    }

    private void StartStop()
    {
        StartStopCommand.RaiseCanExecuteChanged();
        Toggle();
    }
}