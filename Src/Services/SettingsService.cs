﻿using System.IO;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using Prism.Events;
using Serilog;
using TrayPinger.Events;
using TrayPinger.Helpers;
using TrayPinger.Models;
using Wpf.Tr;

namespace TrayPinger.Services;

public class SettingsService
{
    private readonly DataSeedHelper _seedHelper;
    private readonly IEventAggregator _eventAggregator;
    private readonly TranslateManager _translateManager;

    public SettingsModel Current { get; private set; }

    public SettingsService(DataSeedHelper seedHelper, IEventAggregator eventAggregator, TranslateManager translateManager)
    {
        _seedHelper = seedHelper;
        _eventAggregator = eventAggregator;
        _translateManager = translateManager;
    }

    public async Task Load()
    {
        Current = await LoadInternalAsync();
        _eventAggregator.GetEvent<SettingsLoadedEvent>().Publish(Current);
    }

    private async Task<SettingsModel> LoadInternalAsync()
    {
        try
        {
            var path = DirectoryHelper.GetSettingsFullPath();
            var json = await File.ReadAllTextAsync(path);
            var options = GetJsonSerializerOptions();

            var settings = JsonSerializer.Deserialize<SettingsModel>(json, options);
            return settings;
        }
        catch (Exception ex)
        {
            Log.Logger.Warning(ex, _translateManager.Translate("SettingsFileCorruptedDefaultLoadedError"));

            _eventAggregator.GetEvent<ApplicationFirstRunEvent>().Publish();
            var defaultSettings = await CreateAndSaveDefaultSettings();
            return defaultSettings;
        }
    }

    private JsonSerializerOptions GetJsonSerializerOptions()
    {
        var options = new JsonSerializerOptions
        {
            WriteIndented = true,
            DefaultIgnoreCondition = JsonIgnoreCondition.Never,
            Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping,
        };

        return options;
    }

    private async Task<SettingsModel> CreateAndSaveDefaultSettings()
    {
        var settings = _seedHelper.GetDefaultSettings();
        await SaveInternalAsync(settings);
        return settings;
    }

    public async Task SaveAsync(SettingsModel model)
    {
        Current = model;
        await SaveAsync();
    }

    public async Task SaveAsync()
    {
        await SaveInternalAsync(Current);
        _eventAggregator.GetEvent<SettingsSavedEvent>().Publish(Current);
    }

    private async Task SaveInternalAsync(SettingsModel settings)
    {
        try
        {
            var path = DirectoryHelper.GetSettingsFullPath();
            var options = GetJsonSerializerOptions();
            var json = JsonSerializer.Serialize(settings, options);
            await File.WriteAllTextAsync(path, json);
        }
        catch (Exception ex)
        {
            Log.Logger.Error(ex, _translateManager.Translate("SettingsSaveError"));
        }
    }
}