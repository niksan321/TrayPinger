﻿using System.Windows;
using System.Windows.Controls.Primitives;
using Hardcodet.Wpf.TaskbarNotification;
using Prism.Ioc;
using Prism.Regions;
using TrayPinger.Controls;
using TrayPinger.Enums;
using TrayPinger.Extensions;
using TrayPinger.Resourses;
using TrayPinger.ViewModels;

namespace TrayPinger.Services;

public class TrayService
{
    private readonly IContainerProvider _container;
    private readonly IRegionManager _regionManager;
    private readonly SettingsService _settingsService;
    private TaskbarIcon _taskBarIcon;
    private NotifyIconViewModel _notifyIconViewModel;
    private readonly PrismApplicationEx _app;

    public TrayService(IContainerProvider container, IRegionManager regionManager, SettingsService settingsService)
    {
        _container = container;
        _regionManager = regionManager;
        _settingsService = settingsService;

        _app = (PrismApplicationEx)Application.Current;
        _app.Exit += _application_Exit;
    }

    public void Init()
    {
        _notifyIconViewModel = _container.Resolve<NotifyIconViewModel>();
        _taskBarIcon = (TaskbarIcon)_app.FindResource("TaskBarIcon");
        _taskBarIcon!.DataContext = _notifyIconViewModel;
    }

    private void _application_Exit(object sender, ExitEventArgs e)
    {
        _taskBarIcon.Dispose();
    }

    public void Hide()
    {
        _container.CurrentScope.IsAttached = false;
        _app.MainWindow!.Close();

        _regionManager.ClearRegions();
        _notifyIconViewModel.Invalidate();
    }

    public void Show()
    {
        var newScope = _container.CreateScope();
        newScope.IsAttached = true;

        _app.Start();
        _notifyIconViewModel.Invalidate();
    }

    public void SetDefaultIcon()
    {
        _taskBarIcon.Icon = Assets.TryPinger64;
    }

    public void SetDisabledIcon()
    {
        _taskBarIcon.Icon = Assets.TryPingerDisabled64;
    }

    public void SetErrorIcon()
    {
        _taskBarIcon.Icon = Assets.TryPingerError64;
    }

    public void ShowErrorBalloon(string text)
    {
        var balloon = new MessageBalloon(MessageType.Error, text);
        _taskBarIcon.ShowCustomBalloon(balloon, PopupAnimation.Slide, 1000 * _settingsService.Current.TimeToShowPopupSec);
    }

    public void ShowInfoBalloon(string text)
    {
        var balloon = new MessageBalloon(MessageType.Info, text);
        _taskBarIcon.ShowCustomBalloon(balloon, PopupAnimation.Slide, 1000 * _settingsService.Current.TimeToShowPopupSec);
    }
}