﻿using Prism.Mvvm;

namespace TrayPinger.Services;

public class MenuService : BindableBase
{
    private string _mainTitleId;
    public string MainTitleId
    {
        get => _mainTitleId;
        set => SetProperty(ref _mainTitleId, value);
    }
}