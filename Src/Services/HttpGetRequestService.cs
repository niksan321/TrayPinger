﻿using System.Net;
using System.Net.Http;
using Serilog;
using TrayPinger.Extensions;
using TrayPinger.Models;

namespace TrayPinger.Services;

public class HttpGetRequestService : IRequestService
{
    private readonly AutoResetEvent _autoEvent = new(true);

    public async Task<RequestReplay> SendAsync(IRequestTarget target)
    {
        if (!_autoEvent.WaitOne(0)) return RequestReplay.Sending;

        try
        {
            var client = new HttpClient();
            client.Timeout = target.RequestTimeout.ToTimeSpan();
            var replay = await client.GetWithElapsedAsync(target.IpOrAddress);

            return new RequestReplay
            {
                IsSuccess = replay.Item1.StatusCode == HttpStatusCode.OK,
                IsSended = true,
                RoundtripTimeMs = (int)replay.Item2.TotalMilliseconds,
                Message = $"{replay.Item1.StatusCode}"
            };
        }
        catch (Exception ex)
        {
            Log.Error(ex.ToString());
            return RequestReplay.Error(ex);
        }
        finally
        {
            _autoEvent.Set();
        }
    }
}