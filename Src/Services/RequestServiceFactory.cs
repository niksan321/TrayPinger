﻿using TrayPinger.Enums;

namespace TrayPinger.Services;

public class RequestServiceFactory
{
    private readonly PingRequestService _pingRequestService = new();
    private readonly HttpGetRequestService _httpGetRequestService = new();
    private readonly HttpPostRequestService _httpPostRequestService = new();

    public IRequestService GetService(RequestType type)
    {
        switch (type)
        {
            case RequestType.IcmpPing:
                return _pingRequestService;
            case RequestType.HttpGet:
                return _httpGetRequestService;
            case RequestType.HttpPost:
                return _httpPostRequestService;
            default:
                throw new ArgumentOutOfRangeException(nameof(type), type, null);
        }
    }
}