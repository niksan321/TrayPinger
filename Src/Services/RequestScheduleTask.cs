﻿using Prism.Events;
using TrayPinger.Events;
using TrayPinger.Models;
using TrayPinger.ViewModels;
using Timer = System.Threading.Timer;

namespace TrayPinger.Services;

public sealed class RequestScheduleTask : IDisposable
{
    private IRequestService _requestService;
    private readonly IEventAggregator _eventAggregator;
    private readonly RequestServiceFactory _requestServiceFactory;
    private TargetItemViewModel _target;
    private readonly Timer _timer;
    private bool _disposed;

    public RequestScheduleTask(IEventAggregator eventAggregator, RequestServiceFactory requestServiceFactory)
    {
        _eventAggregator = eventAggregator;
        _requestServiceFactory = requestServiceFactory;
        _timer = new Timer(TimerCallback);
    }

    public void InitTarget(TargetItemViewModel target)
    {
        _requestService = _requestServiceFactory.GetService(target.Type);
        _target = target;
        _target.ScheduleTask = this;
    }

    private async void TimerCallback(object state)
    {
        _target.IsRequestSending = true;
        var response = await _requestService.SendAsync(_target);

        _eventAggregator.GetEvent<TargetItemReplayEvent>()
                        .Publish(new TargetItemReplayEventPayload(_target, response));

        InternalTimerStart();
    }

    public void Start()
    {
        if (_target.Enabled)
        {
            InternalTimerStart();
            _target.IsRunning = true;
        }
    }

    public void Stop()
    {
        InternalTimerStop();
        _target.IsRunning = false;
    }

    private void InternalTimerStop()
    {
        _timer.Change(Timeout.Infinite, Timeout.Infinite);
    }

    private void InternalTimerStart()
    {
        //task can be disposed before long request finished
        if (_disposed) return;

        var interval = (long)_target.RequestInterval.ToTimeSpan().TotalMilliseconds;
        _timer.Change(interval, Timeout.Infinite);
    }

    public void Dispose()
    {
        _disposed = true;
        _timer.Dispose();
    }
}