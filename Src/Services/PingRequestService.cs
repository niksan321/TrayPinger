﻿using System.Net.NetworkInformation;
using Serilog;
using TrayPinger.Models;

namespace TrayPinger.Services;

public class PingRequestService : IRequestService
{
    private readonly byte[] _buffer = new byte[32];
    private readonly PingOptions _options = new(32, true);
    private readonly AutoResetEvent _autoEvent = new(true);

    public async Task<RequestReplay> SendAsync(IRequestTarget target)
    {
        if (!_autoEvent.WaitOne(0)) return RequestReplay.Sending;

        try
        {
            using var sender = new Ping();

            var replay = await sender.SendPingAsync(target.IpOrAddress,
                (int)target.RequestTimeout.ToTimeSpan().TotalMilliseconds, _buffer, _options);

            return new RequestReplay
            {
                IsSuccess = replay.Status == IPStatus.Success,
                IsSended = true,
                RoundtripTimeMs = (int)replay.RoundtripTime,
                Message = $"{replay.Status}"
            };
        }
        catch (Exception ex)
        {
            Log.Warning(ex, "SendAsync exception");
            return RequestReplay.Error(ex);
        }
        finally
        {
            _autoEvent.Set();
        }
    }
}