﻿namespace TrayPinger.Constants;

public static class AppConstants
{
    public static class Application
    {
        public const string Name = "TryPinger v1.0";
    }

    public static class Settings
    {
        public const string FileName = "Settings.json";
    }

    public static class Log
    {
        public const string FileName = "Log.txt";
    }
}