﻿using System.Globalization;
using System.Windows;
using DryIoc;
using Mapster;
using MapsterMapper;
using Prism.Ioc;
using Prism.Mvvm;
using Prism.Regions;
using TrayPinger.Constants;
using TrayPinger.Helpers;
using TrayPinger.Managers;
using TrayPinger.Services;
using TrayPinger.ViewModels;
using TrayPinger.Views;
using TrayPinger.Watcher;
using Wpf.Tr;

namespace TrayPinger;

/// <summary>
/// Interaction logic for App.xaml
/// </summary>
public partial class App : PrismApplicationEx
{
    private IRegionManager _regionManager;

    public static Rules DefaultRules => Rules.Default.WithConcreteTypeDynamicRegistrations(reuse: Reuse.Transient)
        .With(Made.Of(FactoryMethod.ConstructorWithResolvableArguments))
        .WithFuncAndLazyWithoutRegistration()
        .WithTrackingDisposableTransients()
        //.WithoutFastExpressionCompiler()
        .WithFactorySelector(Rules.SelectLastRegisteredFactory());

    protected override Rules CreateContainerRules()
    {
        return DefaultRules;
    }

    protected override void RegisterTypes(IContainerRegistry containerRegistry)
    {
        containerRegistry.RegisterScoped<IMapper, ServiceMapper>();

        containerRegistry.RegisterScoped<MenuService>();
        containerRegistry.RegisterScoped<TargetItemWatcher>();
        containerRegistry.RegisterScoped<SchedulerWatcher>();
        containerRegistry.RegisterScoped<SettingsWatcher>();
        containerRegistry.RegisterScoped<WindowManager>();

        containerRegistry.RegisterSingleton<TranslateManager>();
        containerRegistry.RegisterSingleton<TypeAdapterConfig>();
        containerRegistry.RegisterSingleton<SettingsService>();
        containerRegistry.RegisterSingleton<RequestServiceFactory>();
        containerRegistry.RegisterSingleton<SchedulerService>();
        containerRegistry.RegisterSingleton<DataSeedHelper>();
        containerRegistry.RegisterSingleton<TargetItemStorageService>();
        containerRegistry.RegisterSingleton<TrayService>();

        containerRegistry.Register<RequestScheduleTask>();
        containerRegistry.Register<SettingsViewModel>();
        containerRegistry.Register<TargetItemViewModel>();

        containerRegistry.RegisterForNavigation<TargetListView>();
        containerRegistry.RegisterForNavigation<EditTargetView>();
        containerRegistry.RegisterForNavigation<SettingsView>();
        containerRegistry.RegisterForNavigation<MenuView>();

        containerRegistry.RegisterDialog<YesNoDialogView, YesNoDialogViewModel>();

        ViewModelLocationProvider.Register(typeof(SettingsView).ToString(), () =>
        {
            var mapper = Container.Resolve<IMapper>();
            var currentSettings = Container.Resolve<SettingsService>().Current;
            return mapper.Map<SettingsViewModel>(currentSettings);
        });
    }

    protected override Window CreateShell()
    {
        return null;
    }

    protected override void OnStartup(StartupEventArgs e)
    {
        LogHelper.CreateLogger();
        ExceptionsHelper.SetupExceptionHandling(this);

        base.OnStartup(e);
    }

    protected override async void OnInitialized()
    {
        var currentScope = Container.CreateScope();
        currentScope.IsAttached = true;

        Container.Resolve<TypeAdapterConfig>().Scan(GetType().GetAssembly());

        StartSingletonServices();
        InitLanguages();

        await LoadSettingsAsync();
        LoadLanguageFromSettings();

        _regionManager = Container.Resolve<IRegionManager>();
        _regionManager.RegisterViewWithRegion(RegionConstants.Menu, nameof(MenuView));

        var settingsService = Container.Resolve<SettingsService>();
        if (!settingsService.Current.StartMinimized)
        {
            Start();
        }
    }

    private void InitLanguages()
    {
        var langs = new CultureInfo[] { new("ru-RU"), new("en-US") };
        var manager = Container.Resolve<TranslateManager>();

        manager.InitSupportedLanguages(langs);
        manager.RegisterResourceManager(Langs.Langs.ResourceManager);
    }

    private void StartSingletonServices()
    {
        Container.Resolve<TranslateManager>();
        Container.Resolve<TargetItemWatcher>();
        Container.Resolve<SettingsWatcher>();
        Container.Resolve<SchedulerWatcher>();
        Container.Resolve<SchedulerService>();
        Container.Resolve<ApplicationFirstRunWatcher>();

        var trayService = Container.Resolve<TrayService>();
        trayService.Init();
    }

    private void LoadLanguageFromSettings()
    {
        var translationManager = Container.Resolve<TranslateManager>();
        var settingsService = Container.Resolve<SettingsService>();
        var lang = TranslateManager.Languages.First(x => x.Name == settingsService.Current.Language);
        translationManager.CurrentLanguage = lang;
    }

    private async Task LoadSettingsAsync()
    {
        var settingsService = Container.Resolve<SettingsService>();
        await settingsService.Load();
    }
}