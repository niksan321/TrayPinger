﻿using System.Media;
using MapsterMapper;
using Prism.Events;
using Prism.Ioc;
using TrayPinger.Events;
using TrayPinger.Extensions;
using TrayPinger.Models;
using TrayPinger.Resourses;
using TrayPinger.Services;
using TrayPinger.ViewModels;
using Wpf.Tr;

namespace TrayPinger.Watcher;

internal class TargetItemWatcher
{
    private readonly IEventAggregator _eventAggregator;
    private readonly SchedulerService _schedulerService;
    private readonly SettingsService _settingsService;
    private readonly TrayService _trayService;
    private readonly TargetItemStorageService _storageService;
    private readonly IContainerProvider _container;
    private readonly IMapper _mapper;
    private readonly TranslateManager _translationManager;

    public TargetItemWatcher(IEventAggregator eventAggregator, IContainerProvider container, IMapper mapper, TranslateManager translationManager,
        SchedulerService schedulerService, SettingsService settingsService, TrayService trayService, TargetItemStorageService storageService)
    {
        _eventAggregator = eventAggregator;
        _schedulerService = schedulerService;
        _settingsService = settingsService;
        _trayService = trayService;
        _storageService = storageService;
        _container = container;
        _mapper = mapper;
        _translationManager = translationManager;

        eventAggregator.GetEvent<TargetItemRemovedEvent>().Subscribe(OnTargetRemoved);
        eventAggregator.GetEvent<TargetItemAddedEvent>().Subscribe(OnTargetAdded);
        eventAggregator.GetEvent<TargetItemChangedEvent>().Subscribe(OnTargetChanged);
        eventAggregator.GetEvent<TargetItemFailChangedEvent>().Subscribe(OnTargetFailsChanged);
        eventAggregator.GetEvent<TargetItemReplayEvent>().Subscribe(OnTargetReplay);
        eventAggregator.GetEvent<TargetItemsStorageInitEvent>().Subscribe(OnTargetStorageInit);
    }

    private void OnTargetStorageInit(TargetItemViewModel[] targets)
    {
        foreach (var target in targets)
        {
            AddTargetToScheduler(target);
        }
    }

    private void OnTargetReplay(TargetItemReplayEventPayload payload)
    {
        payload.Target.IsRequestSending = false;
        payload.Target.Message = payload.Replay.Message;

        if (payload.Replay.IsSended)
        {
            payload.Target.LastResponseTimeMs = TimeSpan.FromMilliseconds(payload.Replay.RoundtripTimeMs);
            CalculateIsSuccess(payload.Target, payload.Replay.IsSuccess);
        }
    }

    private void OnTargetFailsChanged()
    {
        var hasError = _storageService.Items.Any(x => !x.IsSuccess);

        UpdateTrayIcon(hasError);
        PlaySound(hasError);
        ShowBalloon();
    }

    private async void OnTargetAdded(TargetItemViewModel target)
    {
        AddTargetToScheduler(target);
        await AddTargetToSettings(target);
    }

    private async void OnTargetRemoved(TargetItemViewModel target)
    {
        RemoveTargetFromScheduler(target);
        await RemoveTargetFromSettings(target);

        OnTargetFailsChanged();
    }

    private async void OnTargetChanged(TargetItemViewModel target)
    {
        UpdateTargetInScheduler(target);
        await UpdateTargetInSettingsAsync(target);
    }

    private void UpdateTargetInScheduler(TargetItemViewModel target)
    {
        RemoveTargetFromScheduler(target);
        AddTargetToScheduler(target);
    }

    private void ShowBalloon()
    {
        var failsTargetsNames = _storageService.Items.Where(x => !x.IsSuccess).Select(x => x.Name).ToArray();
        var failsTargets = string.Join(", ", failsTargetsNames);

        if (failsTargetsNames.Any())
        {
            UiEx.Run(() =>
                _trayService.ShowErrorBalloon(
                    _translationManager.Translate(nameof(Langs.Langs.NoConnectTo), failsTargets)));
        }
        else
        {
            UiEx.Run(() =>
                _trayService.ShowInfoBalloon(_translationManager.Translate(nameof(Langs.Langs.ConnectionRestored))));
        }
    }

    private void CalculateIsSuccess(TargetItemViewModel target, bool isSuccess)
    {
        var oldIsSuccess = target.IsSuccess;

        if (isSuccess)
        {
            target.FailsCount = 0;
            target.IsSuccess = true;
        }
        else
        {
            target.FailsCount++;
            if (target.CountToFail < target.FailsCount)
            {
                target.IsSuccess = false;
            }
        }

        if (oldIsSuccess != target.IsSuccess)
        {
            _eventAggregator.GetEvent<TargetItemFailChangedEvent>().Publish();
        }
    }

    private void PlaySound(bool hasError)
    {
        if (_settingsService.Current.PlayErrorSound && hasError)
        {
            var snd = new SoundPlayer(Assets.ding);
            snd.Play();
        }
    }

    private void UpdateTrayIcon(bool hasError)
    {
        if (hasError)
        {
            _trayService.SetErrorIcon();
        }
        else if (_schedulerService.IsRunning)
        {
            _trayService.SetDefaultIcon();
        }
        else
        {
            _trayService.SetDisabledIcon();
        }
    }

    private void AddTargetToScheduler(TargetItemViewModel target)
    {
        var task = _container.Resolve<RequestScheduleTask>();
        task.InitTarget(target);
        _schedulerService.AddTask(task);
    }

    private void RemoveTargetFromScheduler(TargetItemViewModel target)
    {
        _schedulerService.RemoveTask(target.ScheduleTask);
    }

    private async Task UpdateTargetInSettingsAsync(TargetItemViewModel target)
    {
        var oldModel = _settingsService.Current.TargetItems.First(x => x.Id == target.Id);
        var newModel = _mapper.Map<TargetItemModel>(target);

        _settingsService.Current.TargetItems.Remove(oldModel);
        _settingsService.Current.TargetItems.Add(newModel);

        await _settingsService.SaveAsync();
    }

    private async Task AddTargetToSettings(TargetItemViewModel target)
    {
        var oldModel = _settingsService.Current.TargetItems.Find(x => x.Id == target.Id);
        if (oldModel != null) return;

        var model = _mapper.Map<TargetItemModel>(target);
        _settingsService.Current.TargetItems.Add(model);

        await _settingsService.SaveAsync();
    }

    private async Task RemoveTargetFromSettings(TargetItemViewModel target)
    {
        var model = _settingsService.Current.TargetItems.First(x => x.Id == target.Id);
        _settingsService.Current.TargetItems.Remove(model);

        await _settingsService.SaveAsync();
    }
}