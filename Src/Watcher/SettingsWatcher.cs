﻿using MapsterMapper;
using Prism.Events;
using TrayPinger.Events;
using TrayPinger.Helpers;
using TrayPinger.Models;
using TrayPinger.Services;
using TrayPinger.ViewModels;

namespace TrayPinger.Watcher;

public class SettingsWatcher
{
    private readonly IMapper _mapper;
    private readonly SchedulerService _schedulerService;
    private readonly TargetItemStorageService _storageService;
    private readonly TrayService _trayService;

    public SettingsWatcher(IEventAggregator eventAggregator, IMapper mapper,
        SchedulerService schedulerService, TargetItemStorageService storageService, TrayService trayService)
    {
        _mapper = mapper;
        _schedulerService = schedulerService;
        _storageService = storageService;
        _trayService = trayService;
        eventAggregator.GetEvent<SettingsLoadedEvent>().Subscribe(OnSettingsLoaded);
        eventAggregator.GetEvent<SettingsSavedEvent>().Subscribe(OnSettingsSaved);
    }

    private void OnSettingsSaved(SettingsModel obj)
    {
        StartupHelper.SetStartup(obj.RunOnStartUp);
    }

    private void OnSettingsLoaded(SettingsModel settings)
    {
        var viewTargetItems = _mapper.Map<TargetItemViewModel[]>(settings.TargetItems);
        _storageService.Init(viewTargetItems);

        if (settings.AutoStartPing)
        {
            _schedulerService.Run();
            _trayService.SetDefaultIcon();
        }
        else
        {
            _trayService.SetDisabledIcon();
        }
    }
}