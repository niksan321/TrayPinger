﻿using Prism.Events;
using Prism.Ioc;
using TrayPinger.Events;
using Wpf.Tr;

namespace TrayPinger.Watcher;

public class ApplicationFirstRunWatcher
{
    private readonly IContainerProvider _container;

    public ApplicationFirstRunWatcher(IEventAggregator eventAggregator, IContainerProvider container)
    {
        _container = container;
        eventAggregator.GetEvent<ApplicationFirstRunEvent>().Subscribe(OnApplicationFirstRun);
    }

    private void OnApplicationFirstRun()
    {
        var translationManager = _container.Resolve<TranslateManager>();
        translationManager.SetLanguageFromSystem();
    }
}