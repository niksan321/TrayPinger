﻿using Prism.Events;
using TrayPinger.Events;
using TrayPinger.Services;

namespace TrayPinger.Watcher;

public class SchedulerWatcher
{
    private readonly TrayService _trayService;
    private readonly TargetItemStorageService _storageService;

    public SchedulerWatcher(IEventAggregator eventAggregator, TrayService trayService, TargetItemStorageService storageService)
    {
        _trayService = trayService;
        _storageService = storageService;
        eventAggregator.GetEvent<SchedulerIsRunningChangedEvent>().Subscribe(OnIsRunningChanged);
    }

    private void OnIsRunningChanged(bool isRunning)
    {
        UpdateTrayIcon(isRunning);

        if (!isRunning)
        {
            ResetTargetItems();
        }
    }

    private void ResetTargetItems()
    {
        foreach (var item in _storageService.Items)
        {
            item.Reset();
        }
    }

    private void UpdateTrayIcon(bool isRunning)
    {
        if (isRunning)
        {
            _trayService.SetDefaultIcon();
        }
        else
        {
            _trayService.SetDisabledIcon();
        }
    }
}