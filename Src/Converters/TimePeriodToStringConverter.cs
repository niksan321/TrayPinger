﻿using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Data;
using TrayPinger.Models;
using Wpf.Tr;

namespace TrayPinger.Converters;

[ValueConversion(typeof(TimeSpan), typeof(string))]
internal class TimePeriodToStringConverter : IValueConverter
{
    private readonly TranslateManager _translateManager = TranslateManager.Instance;

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value == null) return null;

        TimeSpan ts;
        var sb = new StringBuilder();

        if (value is TimeSpan timeSpan)
        {
            ts = timeSpan;
        }
        else
        {
            ts = ((TimePeriod)value).ToTimeSpan();
        }

        if (ts.Days > 0)
        {
            var suf = _translateManager.Translate("TimePeriodDay");
            sb.Append($"{ts.Days}{suf}");
        }
        if (ts.Hours > 0)
        {
            var suf = _translateManager.Translate("TimePeriodHour");
            sb.Append($" {ts.Hours}{suf}");
        }
        if (ts.Minutes > 0)
        {
            var suf = _translateManager.Translate("TimePeriodMinutes");
            sb.Append($" {ts.Minutes}{suf}");
        }
        if (ts.Seconds > 0)
        {
            var suf = _translateManager.Translate("TimePeriodSecond");
            sb.Append($" {ts.Seconds}{suf}");
        }
        if (ts.Milliseconds > 0)
        {
            var suf = _translateManager.Translate("TimePeriodMillisecond");
            sb.Append($" {ts.Milliseconds}{suf}");
        }

        return sb.ToString();
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return DependencyProperty.UnsetValue;
    }
}