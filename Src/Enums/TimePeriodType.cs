﻿using Wpf.Tr;

namespace TrayPinger.Enums;

public enum TimePeriodType
{
    [Translate("TimePeriodMillisecond")]
    Milliseconds,
    [Translate("TimePeriodSecond")]
    Seconds,
    [Translate("TimePeriodMinutes")]
    Minutes,
    [Translate("TimePeriodHour")]
    Hours,
    [Translate("TimePeriodDay")]
    Days
}