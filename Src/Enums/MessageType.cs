﻿using Wpf.Tr;

namespace TrayPinger.Enums;

public enum MessageType
{
    [Translate("Information")]
    Info,
    [Translate("Warning")]
    Warning,
    [Translate("Error")]
    Error
}