﻿using Wpf.Tr;

namespace TrayPinger.Enums;

public enum RequestType
{
    [Translate("RequestTypePing")]
    IcmpPing,
    [Translate("RequestTypeGet")]
    HttpGet,
    [Translate("RequestTypePost")]
    HttpPost
}